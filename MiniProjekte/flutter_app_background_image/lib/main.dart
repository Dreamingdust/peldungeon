import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(
        title: 'Nature: Time to relax',
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          children: [
            Image.asset(
              'image/icon1.jpg',
              width: 150.0,
              height: 200.0,
              fit: BoxFit.cover,
            ),
            Text('Bird'),
            Image.asset(
              'image/icon2.jpg',
              width: 150.0,
              height: 300.0,
              fit: BoxFit.fitWidth,
            ),
            Text('Meditation'),
            Image.asset(
              'image/icon3.jpg',
              width: 150.0,
              height: 400.0,
              fit: BoxFit.cover,
            ),
            Text('Tree'),
            Image.asset(
              'image/icon5.jpg',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Mountain'),
            Image.asset(
              'image/icon4.jpg',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Sunset'),
            Image.asset(
              'image/icon6.jpg',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Elephant'),
            Image.asset(
              'image/icon7.jpg',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Sakura'),
            Image.asset(
              'image/icon8.gif',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Sunset'),
            Image.asset(
              'image/icon9.gif',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Sea'),
            Image.asset(
              'image/icon10.gif',
              width: 150.0,
              height: 256.0,
              fit: BoxFit.cover,
            ),
            Text('Ocean')
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Zitate()),
          );
        },
        tooltip: 'Search',
        child: Icon(Icons.help),
      ),
    );
  }
}

class Zitate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Zitate"),

      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('image/image.png'),
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: Text(
                '  Die Natur ist... '
                'unerbittlich und unveränderlich, und es ist ihr gleichgültig, '
                'ob die verborgenen Gründe und Arten ihres Handelns dem Menschen verständlich sind oder nicht. Wenn man die Natur wahrhaft liebt, '
                'so findet man es überall schön. Die Schwärmerei für die Natur kommt von der Unbewohnbarkeit der Städte.',
                style: TextStyle(
                    color: Colors.brown[800],
                    fontWeight: FontWeight.normal,
                    fontStyle: FontStyle.italic,
                    letterSpacing: 0.5,
                    fontSize: 25)),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Go back!'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
