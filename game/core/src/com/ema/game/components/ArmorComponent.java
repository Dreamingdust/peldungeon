package com.ema.game.components;

import com.badlogic.ashley.core.Component;

public class ArmorComponent implements Component {
    public int id = 0;
    public String name = "";
    public int level;
    public String armor;
    public String bonus_hp;
}
