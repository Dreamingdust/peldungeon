package com.ema.game.components;

import com.badlogic.ashley.core.Component;

public class PotionComponent implements Component {
    public int id = 0;
    public String name = "";
    public String health;
    public String strength;
    public String armor;
}
